import React, { Component } from 'react'
import Banner from './Banner'
import Product from './Product'

export default class Body extends Component {
  render() {
    return (
      <div>
          <Banner></Banner>
          <Product></Product>
      </div>
    )
  }
}
