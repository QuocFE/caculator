import React, { Component } from 'react'

export default class Banner extends Component {
    render() {
        return (
            <div>
                <div className='container py-5 px-3'>
                    <h1>A Warm Welcome</h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, praesentium veniam ad molestias, repudiandae facilis eaque minus sunt tempora eum sed quisquam magni corrupti alias, eligendi iure obcaecati dolores corporis. </p>
                    <button type="button" class="btn btn-primary">Call To Action</button>
                </div>
            </div>
        )
    }
}
