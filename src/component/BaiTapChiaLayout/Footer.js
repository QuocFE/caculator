import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <footer className='bg-dark py-4'>
        <p className='text-center'>Copyright @ Your Website 2019</p>
      </footer>
    )
  }
}
