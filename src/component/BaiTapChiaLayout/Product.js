import React, { Component } from 'react'

export default class Product extends Component {
  render() {
    return (
      <div>
          <div className='container'>
              <div className='row'>
                  <div className='col-3'>
                      <div className='item'>
                            <div className="card">
                                <img src="..." className="card-img-top" alt="..." />
                                <div className="card-body text-center">
                                    <h5 className="card-title">Card title</h5>
                                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" className="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>

                      </div>
                  </div>
                  <div className='col-3'>
                      <div className='item'>
                            <div className="card">
                                <img src="..." className="card-img-top" alt="..." />
                                <div className="card-body text-center">
                                    <h5 className="card-title">Card title</h5>
                                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" className="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>

                      </div>
                  </div>
                  <div className='col-3'>
                      <div className='item'>
                            <div className="card">
                                <img src="..." className="card-img-top" alt="..." />
                                <div className="card-body text-center">
                                    <h5 className="card-title">Card title</h5>
                                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" className="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>

                      </div>
                  </div>
                  <div className='col-3'>
                      <div className='item'>
                            <div className="card">
                                <img src="..." className="card-img-top" alt="..." />
                                <div className="card-body text-center">
                                    <h5 className="card-title">Card title</h5>
                                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" className="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>

                      </div>
                  </div>
              </div>
          </div>
      </div>
    )
  }
}
