import React, { Component } from 'react'

export default class DataBinding extends Component {
    renderImg=()=>{
     return <img src='https://d9i9nmwzijaw9.cloudfront.net/450/774/576/-29996968-1tfd7tc-gpggmmc5d0og3a0/original/image.jpg' alt='covid'></img>
    }
    renderMultiComponent=()=>{
        let virrus={
            id:"Covid 19",
            age:"corona",
            alias:"SARS-Cov 2",
            desc:"chinese",
        }
        return( 
            <div className="card" >
                <img  src='https://d9i9nmwzijaw9.cloudfront.net/450/774/576/-29996968-1tfd7tc-gpggmmc5d0og3a0/original/image.jpg' alt='covid' onClick={()=>{
                    this.openVideo();
                }}/>
                <div className="card-body">
                    <h5 className="card-title">{virrus.id}</h5>
                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href="#" className="btn btn-primary">Go somewhere</a>
                </div>
            </div>

        )

    }
    openVideo=()=>{
        alert("hello");
    }
  render() {
    return (
      <div>
          <h3>Binding Function</h3>
          {this.renderImg()}
          {this.renderMultiComponent()}
      </div>
    )
  }
}
