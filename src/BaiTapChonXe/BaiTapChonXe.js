import React, { Component } from 'react'

export default class BaiTapChonXe extends Component {
    state = {
        imgP:require("../assets/carImg/CarBasic/products/black-car.jpg")
    }
    renderCar(newImg){
        console.log(newImg)
        let newState={
            imgP:newImg
        }
        this.setState(newState)
    }

  render() {
      let styleImg={
          width:"60px",
          height:"60px"
      }
      let item={
          cursor:"pointer"
      }
    return (
      <div>
          <div className='container-fluid'>
              <div className='row'>
                  <div className='col-8'>
                     <img className='w-100' src={this.state.imgP} alt="black-car"/>
                  </div>
                  <div className='col-4'>
                        <div className="card">
                            <div className="card-header text-primary">
                                Exterior Color
                            </div>
                            <ul className="list-group list-group-flush p-3">
                                <li style={item} className="list-group-item border mb-2 d-flex align-items-center" onClick={()=>{
                                    this.renderCar(require('../assets/carImg/CarBasic/products/black-car.jpg'));
                                }}>
                                    <img style={styleImg} src={require("../assets/carImg/CarBasic/icons/icon-black.jpg")}/>
                                    <div className='text ml-2'>
                                        <h5>Crystal Black</h5>
                                        <p className='m-0'>Peari</p>
                                    </div>
                                </li>
                                <li style={item} className="list-group-item border mb-2 d-flex align-items-center" onClick={()=>{
                                      this.renderCar(require("../assets/carImg/CarBasic/products/steel-car.jpg"));
                                }}>
                                    <img style={styleImg} src={require("../assets/carImg/CarBasic/icons/icon-steel.jpg")}/>
                                    <div className='text ml-2'>
                                        <h5>Modern Steel</h5>
                                        <p className='m-0'>Metallic</p>
                                    </div>
                                </li>
                                <li style={item} className="list-group-item border mb-2 d-flex align-items-center" onClick={()=>{
                                      this.renderCar(require("../assets/carImg/CarBasic/products/silver-car.jpg"));
                                }}>
                                    <img style={styleImg} src={require("../assets/carImg/CarBasic/icons/icon-silver.jpg")}/>
                                    <div className='text ml-2'>
                                        <h5>Luner Silver</h5>
                                        <p className='m-0'>Metallic</p>
                                    </div>
                                </li>
                                <li style={item} className="list-group-item border mb-2 d-flex align-items-center" onClick={()=>{
                                      this.renderCar(require("../assets/carImg/CarBasic/products/red-car.jpg"));
                                }}>
                                    <img style={styleImg} src={require("../assets/carImg/CarBasic/icons/icon-red.jpg")}/>
                                    <div className='text ml-2'>
                                        <h5>Rallye Red</h5>
                                        <p className='m-0'>Metallic</p>
                                    </div>
                                </li>
                            </ul>
                        </div>

                  </div>
              </div>
          </div>
      </div>
    )
  }
}
