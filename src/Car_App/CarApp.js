import React, { Component } from 'react'
import dataVehicles from '../Data/arrayFeatures.json'
import dataWheels from '../Data/wheels.json'
import '../Data/wheels.json'
export default class CarApp extends Component {
  renderIconColor() {
    return dataVehicles.map((vehicle, index) => {
      return (
        <li key={index} style={{ cursor: "pointer" }} className="list-group-item border mb-2 d-flex align-items-center" onClick={() => {
          this.chooseVehicle(vehicle)
        }}>
          <img style={{ height: "70px" }} className='' src={vehicle.img} />
          <div className='text ml-2'>
            <h6>{vehicle.title}</h6>
            <p className='m-0'>{vehicle.type}</p>
          </div>
        </li>
      )
    })
  }
  renderIconWheels() {
    return dataWheels.map((wheel, index) => {
      return (
        <li key={index} className="list-group-item border mb-2 d-flex align-items-center" onClick={()=>{
          this.chooseWheel(wheel.idWheel)
        }}>
          <img style={{ height: "70px" }} className='' src={wheel.img} />
          <div className='text ml-2'>
            <h6>{wheel.title}</h6>
            <p className='m-0'>{wheel.price}</p>
          </div>
        </li>
      )
    })
  }
  state = {
    carCurrent:{
      "id": 1,
      "title": "Crystal Black",
      "type": "Pearl",
      "img": "./images/icons/icon-black.jpg",
      "srcImg": "images-black/images-black-1/",
      "color": "Black",
      "price": "19,550",
      "engineType": "In-Line 4-Cylinder",
      "displacement": "1996 cc",
      "horsepower": "158 @ 6500 rpm",
      "torque": "138 lb-ft @ 4200 rpm",
      "redline": "6700 rpm",
      "wheels": [
        {
          "idWheel": 1,
          "srcImg": "images-black/images-black-1/"
        },
        {
          "idWheel": 2,
          "srcImg": "images-black/images-black-2/"
        },
        {
          "idWheel": 3,
          "srcImg": "images-black/images-black-3/"
        }
      ]
    },
  }
  chooseVehicle(newVehicle) {
    this.setState({
      carCurrent:newVehicle,
    });
  }
  componentDidMount() {
    //Đây là phương thức có sẵn của component tự động thực thi sau khi render được goi;
    //Lưu ý hàm này chỉ chạy được một lần sau khi render được thực thi
    let tagScript = document.createElement('script');
    tagScript.src = 'https://cdn.scaleflex.it/plugins/js-cloudimage-360-view/2.7.5/js-cloudimage-360-view.min.js';
    document.querySelector("#appendScript").appendChild(tagScript);
  }
  componentDidUpdate(){
        //HÀm này chạy sau khi state thay đổi. Do vậy
    //Nó sẽ tự kích hoạt sau render
    //Lưu ý không được phép set State tại component nàyư\
    document.querySelector("#carCurrent").innerHTML='';
    let tagScript = document.createElement('script');
    tagScript.src = 'https://cdn.scaleflex.it/filerobot/js-cloudimage-360-view/v2.0.0.lazysizes.min.js';
    document.querySelector("#appendScript").appendChild(tagScript);
  }
  chooseWheel(id){
    let object=this.state.carCurrent.wheels.find((item)=>{
      return item.idWheel===id
    })
    if(object!==-1){
      this.setState({
        carCurrent:{...this.state.carCurrent,srcImg:object.srcImg}
      })
    }
    
  }
  render() {
    let styleCar = {
      height: "360px",
      objectFit: "cover"
    }

    return (
      <div>
        <h1 className='bg-dark text-white text-center'>App Try Vehicles Online</h1>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-8'>
              <div className='car-infor'>
                <div id='carCurrent' className="cloudimage-360" data-folder={"./images/"+this.state.carCurrent.srcImg} data-filename="civic-{index}.jpg" data-amount-x={8} />
                <div id='appendScript'></div>
                <div className="card mt-1">
                  <div className="card-header">
                    Exterior Color
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">See More LX Features</h5>
                    <div className='detail d-flex alight-item-center'>
                      <div className="card flex-grow-1">
                        <ul className="list-group list-group-flush">
                          <li className="list-group-item font-weight-bold">Color</li>
                          <li className="list-group-item font-weight-bold">Price</li>
                          <li className="list-group-item font-weight-bold">Engine Type</li>
                          <li className="list-group-item font-weight-bold">Displacement</li>
                          <li className="list-group-item font-weight-bold">Horsepower (SAE net)</li>
                          <li className="list-group-item font-weight-bold">Torque (SAE net)</li>
                          <li className="list-group-item font-weight-bold">Redline</li>
                        </ul>
                      </div>
                      <div className="card flex-grow-1">
                        <ul className="list-group list-group-flush">
                          <li className="list-group-item ">{this.state.carCurrent.color}</li>
                          <li className="list-group-item ">{this.state.carCurrent.price}</li>
                          <li className="list-group-item ">{this.state.carCurrent.engineType}</li>
                          <li className="list-group-item ">{this.state.carCurrent.displacement}</li>
                          <li className="list-group-item ">{this.state.carCurrent.horsepower}</li>
                          <li className="list-group-item ">{this.state.carCurrent.torque}</li>
                          <li className="list-group-item ">{this.state.carCurrent.redline}</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div className='col-4'>
              <div className="card card-color mb-3">
                <div className="card-header text-primary">
                  Exterior Color
                </div>
                <ul className="list-group list-group-flush p-3">
                  {this.renderIconColor()}
                </ul>
              </div>
              <div className="card card-wheel">
                <div className="card-header text-primary">
                  Wheels
                </div>
                <ul className="list-group list-group-flush p-3">
                  {this.renderIconWheels()}
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>
    )
  }
}
