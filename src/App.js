
import './App.css';
import BaiTapChonXe from './BaiTapChonXe/BaiTapChonXe';
import BaiTapRenderFilm from './BaiTapRenderFilms/BaiTapRenderFilm';
import CaculatorApp from './CaculatorApp/CaculatorApp';
import CarApp from './Car_App/CarApp';
import BaiTapChiaLayout from './component/BaiTapChiaLayout/BaiTapChiaLayout';
import DataBinding from './component/DataBinding/DataBinding';
import RenderCondition from './component/RenderCondition/RenderCondition';
import State from './component/State/State';
import Styling from './component/Styling/Styling';
import GlassesApp from './Glasses_App/GlassesApp';

function App() {
  return (
    <div className="App">
     {/* <BaiTapChiaLayout></BaiTapChiaLayout> */}
     {/* <DataBinding></DataBinding> */}
     {/* <RenderCondition></RenderCondition> */}
     {/* <State></State> */}
     {/* <Styling></Styling> */}
     {/* <BaiTapChonXe></BaiTapChonXe> */}
     {/* <BaiTapRenderFilm></BaiTapRenderFilm> */}
     {/* <GlassesApp></GlassesApp> */}
     {/* <CarApp></CarApp> */}
     <CaculatorApp></CaculatorApp>
    </div>
  );
}

export default App;
