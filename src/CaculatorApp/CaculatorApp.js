import React, { Component } from 'react'
import './styleCaculator.css'
export default class CaculatorApp extends Component {
    state = {
        display: 0,
        preNumber: 0,
        operator: '',
    }
    reset() {
        this.setState({
            display: 0,
            preNumber: 0,
            operator: '',
        })
    }
    changeNumber(value) {
        let newNumber = this.state.display.toString();
        if (newNumber == "0" && value !== '.') {
            newNumber = value.toString();
        } else {
            if (value == '.') {
                if (newNumber.search(/[.]/) !== -1) {
                    return
                } else {
                    newNumber += value.toString();
                }
            } else {
                newNumber += value.toString();
            }
        }
        this.setState({
            ...this.state, display: newNumber
        })

    }
    changeOperator(newOperator) {
        let number = this.state.display;
        this.setState({
            display: 0,
            preNumber: number,
            operator: newOperator,
        })
    }
    caculate() {
        let preNumber = Number(this.state.preNumber);
        let nextNumber = Number(this.state.display);
        let result = 0;
        switch (this.state.operator) {
            case '+':
                result = preNumber + nextNumber;
                break;
            case '-':
                result = preNumber - nextNumber;
                break;
            case '*':
                result = preNumber * nextNumber;
                break;
            case '/':
                if (nextNumber !== 0) {
                    result = preNumber / nextNumber;
                } else {
                    result = "cannot devide by zero"
                }
                break;
            default:

        }
        this.setState({
            ...this.state, display: result
        })


    }
    render() {
        return (
            <div style={{ backgroundImage: 'url(./imagesCaculator/black.jpg)' }} className='background'>
                <h1 className='text-center Header' >Caculator App Online</h1>
                <div className='content px-3'>
                    <div className='headerCaculator'>
                        <input id='value' type={'text'} value={this.state.display}></input>
                    </div>
                    <div className='body'>
                        <table className='w-100 mt-4'>
                            <tr className="mt-3" role="group" >
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeNumber(7)
                                }}>7</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeNumber(8)
                                }}>8</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeNumber(9)
                                }}>9</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeOperator('/')
                                }}>/</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.reset()
                                }}>CE</button></td>
                            </tr>
                            <tr className="mt-3" role="group" >
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeNumber(4)
                                }}>4</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeNumber(5)
                                }}>5</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeNumber(6)
                                }}>6</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeOperator('*')
                                }}>*</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={()=>{
                                    this.reset()
                                }}>C</button></td>
                            </tr>
                            <tr className="mt-3" role="group" >
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeNumber(1)
                                }}>1</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeNumber(2)
                                }}>2</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeNumber(3)
                                }}>3</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeOperator('-')
                                }}>-</button></td>
                                <td rowSpan={2}><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.caculate()
                                }}>=</button></td>
                            </tr>
                            <tr className="mt-3" role="group" >
                                <td colSpan={2}><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeNumber(0)
                                }}>0</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeNumber('.')
                                }}>.</button></td>
                                <td><button type="button" className="btn btn-secondary" onClick={() => {
                                    this.changeOperator('+')
                                }}>+</button></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}
