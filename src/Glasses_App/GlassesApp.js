import React, { Component } from 'react'
import glassCss from './GlassApp.module.css'
import glassData from '../Data/dataGlasses.json'
export default class GlassApp
    extends Component {
    renderGlasses() {
        return glassData.map((glass, index) => {
            return <div key={index} className='col-2 p-3' style={{ cursor: "pointer" }}>
                <img className='w-100' src={glass.url} alt={glass.name} onClick={() => {
                    this.tryGlass(glass.url, glass.name, glass.desc);
                }} />
            </div>
        })
    }
    tryGlass(url, name, desc) {
        let newState = {
            glassImageOfModelLeft: url,
            glassImageOfModelRight: url,
            glassName: name,
            glassDetail: desc,
        }
        this.setState(newState);
    }
    state = {
        glassImageOfModelLeft: "./glassesImage/v1.png",
        glassImageOfModelRight: "",
        glassName: "GUCCI G8850U",
        glassDetail: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
    }
    render() {
        let glassStyle = {
            width: "125px",
            height: "40px",
            position: "absolute",
            left: "25%",
            top: "26%",
            opacity: "0.8",
            transform:"rotate(0)",
            animation:`wearGlass${Date.now()} 2s`,
        }
        let keyFrame=`@keyframes wearGlass${Date.now()}{
            from{
                width: 0;
                transform: rotate(45deg);
            } to{
                width: 125px;
                transform: rotate(0);
            }
        }`
        return (
            <div style={{ backgroundImage: "url(./glassesImage/background.jpg)", height: "2000px" }}>
                <style>{keyFrame}</style>
                <h1 className={glassCss.title}>Try Glasses App Online</h1>
                <div className={glassCss.model}>
                    <div className={glassCss.modelImg}>
                        <div style={glassStyle}>
                            <img className='w-100' src={this.state.glassImageOfModelLeft} />
                        </div>
                        <div className={glassCss.description}>
                            <h3 style={{ fontSize: "20px" }} className='font-weight-bold text-info'>{this.state.glassName}</h3>
                            <p style={{ fontSize: "11px" }} className='m-0 font-weight-bold text-white'>{this.state.glassDetail}</p>
                        </div>
                    </div>
                    <div className={glassCss.modelImg}>
                        <div style={glassStyle}>
                            <img className='w-100' src={this.state.glassImageOfModelRight} />
                        </div>
                    </div>
                </div>
                <div className='glassRegion' style={{ margin: "0 auto", width: "70%" }}>
                    <div className='row mt-5 bg-secondary'>
                        {this.renderGlasses()}

                    </div>
                </div>
            </div>
        )
    }
}
