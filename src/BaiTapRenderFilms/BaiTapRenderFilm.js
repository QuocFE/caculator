import React, { Component } from 'react'
import BodyFilm from './BodyFilm'
import HeaderFilm from './HeaderFilm'
import filmCss from './BaiTapRenderFilm.module.css'
export default class BaiTapRenderFilm extends Component {
  render() {
    return (
      <div className={filmCss.film}>
          <HeaderFilm></HeaderFilm>
          <BodyFilm></BodyFilm>
      </div>
    )
  }
}
