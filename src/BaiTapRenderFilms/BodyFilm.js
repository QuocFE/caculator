import React, { Component } from 'react'
import DataFilm from '../Data/DataFilms.json'
export default class BodyFilm extends Component {
  truncate(text,num){
    if(text.length>num){
      return text.slice(0,num)+"...";
    }
    else{
      return text
    }
  }
  renderFilms() {
    return (
      DataFilm.map((film, index) => {
        return (
          <div className='col-2' key={index}>
            <div className="card border-0">
              <img src={film.hinhAnh} className="card-img-top" alt="..." />
              <div className="card-body bg-dark text-light" >
                <h5 className="card-title">{film.tenPhim}</h5>
                <p className="card-text">{this.truncate(film.moTa,30)}</p>
                <a href="#" className="btn btn-primary">Go somewhere</a>
              </div>
            </div>
          </div>
        )
      })
    )
  }
  render() {
    return (
      <div >
        <div className='container-fluid py-5'>
          <div className='row'>
          {this.renderFilms()}
          </div>
        </div>
      </div>
    )
  }
}
